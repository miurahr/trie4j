import com.github.spotbugs.snom.Confidence

plugins {
    `java-library`
    `maven-publish`
    signing
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
    alias(libs.plugins.nexus.publish)
}


group = "tokyo.northside"
version = "0.9.10_1"
description = "Trie4J"

sourceSets {
    main {
        java {
            srcDir("trie4j/src/main/java")
        }
        resources {
            srcDir("trie4j/src/main/resources")
        }
    }
    test {
        java {
            srcDir("trie4j/src/test/java")
        }
        resources {
            srcDir("trie4j/src/test/resources")
        }
    }
}

val envIsCi = project.hasProperty("envIsCi")
val runSlowTests = project.hasProperty("runSlowTests")

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(libs.junit4)
}

tasks.withType<Test> {
    if (!runSlowTests) {
        exclude("**/slow/**")
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            pom {
                name.set("Trie4j")
                description.set("Various TRIE implementation including DoubleArray and LOUDS by Java.")
                url.set("https://github.com/miurahr/trie4j")
                licenses {
                    license {
                        name.set("Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("miurahr")
                        name.set("Hiroshi Miura")
                        email.set("miurahr@linux.com")
                    }
                    developer {
                        name.set("Takao Nakaguchi")
                        email.set("takao.nakaguchi@gmail.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://github.com/miurahr/trie4j.git")
                    developerConnection.set("scm:git:git://github.com/miurahr/trie4j.git")
                    url.set("https://github.com/miurahr/trie4j")
                }
            }
        }
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

java {
    toolchain {
         languageVersion.set(JavaLanguageVersion.of(11))
    }
    withSourcesJar()
    withJavadocJar()
}

val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}
tasks.withType<Sign> {
    onlyIf { signKey != null }
}

signing {
    when (signKey) {
        "signingKey" -> {
            val signingKey: String? by project
            val signingPassword: String? by project
            useInMemoryPgpKeys(signingKey, signingPassword)
        }
        "signing.keyId" -> {
            // default signatory - do nothing()
            // please set
            // signing.keyId = 0xAAAAAA
            // signing.password = "signature passphrase"
            // secretKeyRingFile = "secring.gpg"
            // e.g. gpg --export-secret-keys > secring.gpg
        }
        "signing.gnupg.keyName" -> {
            useGpgCmd()
        }
    }
    sign(publishing.publications["mavenJava"])
}

val sonatypeUsername: String? by project
val sonatypePassword: String? by project

nexusPublishing.repositories {
    sonatype {
        if (sonatypeUsername != null && sonatypePassword != null) {
            username.set(sonatypeUsername)
            password.set(sonatypePassword)
        } else {
            username.set(System.getenv("SONATYPE_USER"))
            password.set(System.getenv("SONATYPE_PASS"))
        }
    }
}

spotbugs {
    reportLevel.set(Confidence.HIGH)
    excludeFilter.set(project.file("config/spotbugs/exclude.xml"))
    tasks.spotbugsMain {
        reports.create("html") {
            required.set(true)
        }
    }
    tasks.spotbugsTest {
        reports.create("html") {
            required.set(true)
        }
    }
}

tasks.withType<Javadoc> {
    isFailOnError = false
    (options as StandardJavadocDocletOptions).addBooleanOption("Xdoclint:none", true)
}

spotless {
    isEnforceCheck = true
    java {
        target(listOf("src/main/java/**/*.java", "src/test/java/**/*.java"))
        removeUnusedImports()
        palantirJavaFormat()
    }
}
