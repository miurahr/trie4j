/*
 * Copyright 2014 Takao Nakaguchi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.trie4j.test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class IOUtil {

    private static String wikipediaFilename;

    public static String readLine(Path path) throws IOException {
        return Files.readAllLines(path).get(0);
    }

    public static String getWikipediaFilename() throws IOException {
        if (wikipediaFilename == null) {
            Path path = Paths.get("trie4j/data/").resolve(readLine(Paths.get("trie4j/data/wiki")));
            if (!path.toFile().exists()) {
                throw new FileNotFoundException(path.toString());
            }
            wikipediaFilename = path.toString();
        }
        return wikipediaFilename;
    }
}
