package org.trie4j.test;

import java.io.File;
import java.io.FileNotFoundException;
import org.junit.Assert;
import org.junit.Test;

public class WikipediaTitlesTest {
    @Test
    public void test() throws Exception {
        String wikipediaFilename = IOUtil.getWikipediaFilename();
        if (!new File(wikipediaFilename).exists()) throw new FileNotFoundException(wikipediaFilename);
        Iterable<String> itb = new WikipediaTitles(wikipediaFilename);
        int len = 0;
        for (String w : itb) {
            len += w.length();
        }
        itb = null;
        System.out.println(len);
        Assert.assertTrue(len > 100000);
    }
}
